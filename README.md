---
title: "Δομές Δεδομένων 1η Εργασία"
author: "Irakleios Kopitas"
date: "08/12/1019"
geometry: margin=4cm
output: pdf_document
header-includes:
- \setlength{\parindent}{2em}
- \setlength{\parskip}{0em}

---

\begin{center}
\includegraphics[width=50mm]{unipi}
\end{center}

\pagebreak
# Methods Called by greedy

## Select Bidder O(n)

Η Μέθοδος Select Bidder λουπάρει την BuyersList μέχρι να εντοπίσει το BuyersNode με τον μεγαλύτερο συντελεστή
αξίας πρώς μήκος λίστας αντικειμένων.

Αν βρεθεί αγοραστής με μεγαλύτερο συντελεστή από τον bidder
τότε γίνετε εκείνος bidder και επιστρέφετε.

\begin{center}\rule{4.8in}{0.4pt}\end{center}

```java

	private BuyerNode selectBidder(BuyersList buy){
		BuyerNode current = buy.first;
		BuyerNode bidder = buy.first;
		float c = 0;
		float b = 0;

		// Loop list
		while(current != null){
			// value over items for current
			c = (current.value / current.itemsList.size());
			// max value over items
			b = (bidder.value / bidder.itemsList.size());
			// If there is a higher bidder
			if( c > b ){
				// He is now the bidder
				bidder = current;
			}
			current = current.next;
		}
		return bidder;
	}

```

\begin{center}\rule{4.8in}{0.4pt}\end{center}
\pagebreak

## Contains O(m * k)

Η Μέθοδος Contains παίρνει σαν όρισμα μια ItemsList και επιστρέφει true αν είναι
υποσύνολο της.

 Αρχικά υπάρχει μια διπλή λούπα που περνάει από τις τιμές των δύο
λιστών. Όταν Βρεθεί Node με ίδια τιμή αυξάνουμε την τιμή του contained κατά `1`.
Αν το μήκος της ilist είναι ίσο με το contained τότε σημαίνει ότι είναι
υποσύνολο της list και η μέθοδος τερματίζει με true


\begin{center}\rule{4.8in}{0.4pt}\end{center}

```java


	public boolean contains(ItemsList ilist) {
		ItemNode c = this.first;
		ItemNode i = ilist.first;
		int contained = 0;

		// loop ilist
		while(i != null ){
			// loop this.list
			while(c != null ){
				// if values are equal
				if( i.item == c.item ){
					// increment contained
					contained++;
				}
				c = c.next;
			}
			i = i.next;
			// loop c from start again
			c = this.first;
		}
		// if all nodes of ilist are inside this.list
		if ( contained == ilist.nbNodes ){
			return true;
		}else{
			return false;
		}
	}

```
\begin{center}\rule{4.8in}{0.4pt}\end{center}
\pagebreak

## Remove O(m * k)

Η Μέθοδος remove διαγράφει μια λίστα από μία άλλη, Δεν τσεκάρει αν η λίστα υπάρχει
περιέχεται μέσα της, οπότε τρέχετε μετά από την contains

Ξεκινάει διατρέχοντας τις δυο λίστες και όταν βρεθεί Node με ιδια τιμή καλεί
την deleteNode για να το διαγράψει.

\begin{center}\rule{4.8in}{0.4pt}\end{center}

```java

	public void remove(ItemsList ilist) {
		ItemNode c = this.first;
		ItemNode i = ilist.first;

		// Loop ilist
		while(i != null){
			// loop this.list
			while(c != null){
				// If items are equal
				if( i.item == c.item ){
					// Call deleteNode
					deleteNode(c);
					// And break
					break;
				}
				c = c.next;
			}
			i = i.next;
			// loop this.list from the start
			c = this.first;
		}
	}

```

\begin{center}\rule{4.8in}{0.4pt}\end{center}
\pagebreak

## AppendNode O(n)
Η appendNode απλά προσθέτει ένα Node στην αρχή της λίστας και αυξάνει το nbNodes

\begin{center}\rule{4.8in}{0.4pt}\end{center}
```java
	public int append(int item) {
		ItemNode newNode = new ItemNode(item);

		// If the list is empty
		if(this.empty()){
			// New node is the first one
			first = newNode;
		// Else
		}else{
			// Start last from first
			last = first;
			// And loop list until last node
			while ( last.next != null ){
				last = last.next;
			}
			// NewNode is the last node
			last.next = newNode;
		}
		nbNodes++;
		return 0;
	}

```
\begin{center}\rule{4.8in}{0.4pt}\end{center}
\pagebreak


## DeleteNode O(m)
Η Μέθοδος deleteNode διατρέχει την λίστα μέχρι ο current Node να ισούται με τον κόμβο
προς διαγραφή.
Αν δεν τον εντοπίσει και φτάσει στο τέλος της λίστας τότε επιστρέφει ` null `.
Διαφορετικά τον διαγράφει, μειώνει το nbNodes και τον επιστρέφει.

\begin{center}\rule{4.8in}{0.4pt}\end{center}

```java

	private BuyerNode deleteNode(BuyerNode delety){
		if(this.empty()){return null;}
		BuyerNode cur = this.first;
		BuyerNode prev = this.first;

		// Loop list until delety
		while(cur != delety){
			// If cur reaches end of list, exit
			if(cur.next == null){
				return null;
			}else{
				prev = cur;
				cur = cur.next;
			}
		}
		// If cur is first
		if(cur == this.first){
			// Delete it
			this.first = this.first.next;
		// If cur is after some node
		}else{
			// Delete it
			prev.next = cur.next;
		}
		// Decrease nodes
		nbNodes--;
		return delety;
	}

```

\begin{center}\rule{4.8in}{0.4pt}\end{center}
\pagebreak


# The  Greedy() Method

Ξεκινάμε ορίζοντας ένα BuyerNode που θα πάρει την τιμή τού υψηλότερου αγοραστή.
Έπειτα αρχικοποιούμε μια ItemsLists για τα προϊόντα και την BuyersList buyers που
είναι η λίστα πού επιστρέφουμε.

Λουπάρουμε την list και την γεμίζουμε με τιμές μέχρι `m`. Από εκεί θα αφαιρέσουμε
τα προϊόντα των αγοραστών.

Διατρέχουμε την λίστα των αγοραστών και καλούμε την selectBidder για να μας
επιστέψει τον μεγαλύτερο.
Αν η λίστα με τα προϊόντα του υπάρχει στην λίστα μας, τότε διαγράφουμε τα προϊόντα του από
τη λίστα μας και τον μεταφέρουμε στην BuyersList buyers . Διαφορετικά απλά τον διαγράφουμε.

Ο αλγόριθμος θα τρέξει μέχρι να εξαντληθεί η λίστα των αγοραστών


\begin{center}\rule{4.8in}{0.4pt}\end{center}

```java
	public BuyersList greedy(int m) {
		BuyerNode bidder = this.first;
		ItemsList list = new ItemsList();
		BuyersList buyers = new BuyersList();

		// Make the items list
		for(int i=1; i<=m; i++){
			list.append(i);
		}
		// While this.list of buyers is not empty
		while(!this.empty()){
			// Call select Bidder method
			bidder = this.selectBidder(this);
			// If the buyers.( items list ) is contained in list
			if(list.contains(bidder.itemsList)){
				// Remove it
				list.remove(bidder.itemsList);
				// And append the deleted node to the final list of buyers
				buyers.appendNode(this.deleteNode(bidder));
			// Else
			}else{
				// Just remove it
				this.deleteNode(bidder);
			}
		}
		return buyers;
	}
```

\begin{center}\rule{4.8in}{0.4pt}\end{center}
\pagebreak

# Complexity of Greedy Method

Έστω οτι `m` είναι τα αντικείμενα, `n` οι αγοραστές και `k` οι λίστες με τα
προϊόντα των αγοραστών.

Τότε έχουμε :
$$Greedy =  O(m) + O(n) * [ O(n) + O(m * k) + O(m * k) + O(1) + O(n) ] $$

Που αν θεωρήσουμε ότι το `k` μένει σταθερό, γίνετε :

$$Greedy = 2O(n^2) + 2O(m)[O(n)]$$

Και παίρνουμε περιπτώσεις ανάλογα τα αρχεία που δίνουμε, Δηλαδή :

$$Greedy(m = 500) = O(n^2)$$

$$Greedy(n = 2000) = O(m)$$

Άρα η γραφική παράσταση για $m = 500$ πρέπει να είναι παραβολή, ενώ για $n = 2000$
πρέπει να είναι ευθεία.

## Greedy for m200

![m200](timem500.pdf){ width=100% }[h]

## Greedy for n200

![n2000](timep2000.pdf){ width=100% }[h]

# Resaults


## n = 2000 :
```java
- m =  200  avgTime : 361.11  greedy_value : 11789  opt_value : 11971  diff : 1.52%
- m =  400  avgTime : 369.49  greedy_value : 18222  opt_value : 18747  diff : 2.80%
- m =  600  avgTime : 373.42  greedy_value : 22952  opt_value : 23876  diff : 3.87%
- m =  800  avgTime : 393.16  greedy_value : 26106  opt_value : 27099  diff : 3.66%
- m = 1000  avgTime : 417.33  greedy_value : 29964  opt_value : 31084  diff : 3.60%
```

## m = 500 :
```java
- n = 1000  avgTime :    95.71  greedy_value :  5539  opt_value :  5870  diff : 5.64%
- n = 3000  avgTime :   823.82  greedy_value :  9398  opt_value :  9685  diff : 2.96%
- n = 5000  avgTime :  2361.11  greedy_value : 11568  opt_value : 11690  diff : 1.04%
- n = 7000  avgTime :  5178.63  greedy_value : 12663  opt_value : 12743  diff : 0.63%
- n = 9000  avgTime :  9711.09  greedy_value : 13049  opt_value : 13096  diff : 0.36%
```














