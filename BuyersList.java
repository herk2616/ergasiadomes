import java.lang.System;
class ItemNode {
	public int item;
	public ItemNode next;

	public ItemNode(int item) {
		this.item = item;
		this.next = null;
	}	// end of constructor
}

class ItemsList {
	private int nbNodes;
	private ItemNode first;
	private ItemNode last;

	public ItemsList() {
		this.first = null;
		this.last = null;
		this.nbNodes = 0;
	}	// end of constructor

	public int size() {
		return nbNodes;
	}

	public boolean empty() {
		return first == null;
	}

	public boolean contains(ItemsList ilist) {
		ItemNode c = this.first;
		ItemNode i = ilist.first;
		int contained = 0;

		// loop ilist
		while(i != null ){
			// loop this.list
			while(c != null ){
				// if values are equal
				if( i.item == c.item ){
					// increment contained
					contained++;
				}
				c = c.next;
			}
			i = i.next;
			// loop c from start again
			c = this.first;
		}
		// if all nodes of ilist are inside this.list
		if ( contained == ilist.nbNodes ){
			return true;
		}else{
			return false;
		}
	}

	public int append(int item) {
		ItemNode newNode = new ItemNode(item);

		// If the list is empty
		if(this.empty()){
			// New node is the first one
			first = newNode;
		// Else
		}else{
			// Start last from first
			last = first;
			// And loop list until last node
			while ( last.next != null ){
				last = last.next;
			}
			// NewNode is the last node
			last.next = newNode;
		}
		nbNodes++;
		return 0;
	}

	public void remove(ItemsList ilist) {
		ItemNode c = this.first;
		ItemNode i = ilist.first;

		// Loop ilist
		while(i != null){
			// loop this.list
			while(c != null){
				// If items are equal
				if( i.item == c.item ){
					// Call deleteNode
					deleteNode(c);
					// And break
					break;
				}
				c = c.next;
			}
			i = i.next;
			// loop this.list from the start
			c = this.first;
		}
	}


	private ItemNode deleteNode(ItemNode delety){
		// If this.list is empty exit
		if(this.empty()){return null;}
		ItemNode cur = this.first;
		ItemNode prev = this.first;

		// Loop list until delety
		while(cur != delety){
			// If cur reaches end of list, exit
			if(cur.next == null){
				return null;
			// Else continue searching
			}else{
				prev = cur;
				cur = cur.next;
			}
		}
		// You should be here if cur == delety
		// If delety is first,
		if(cur == this.first){
			// Delete it
			this.first = this.first.next;
		// If delety is after some node
		}else{
			// Delete it
			prev.next = cur.next;
		}
		// Decrease Nodes
		nbNodes--;
		return delety;
	}
}

class BuyerNode {

	public int id;
	public int value;
	public ItemsList itemsList;
	public BuyerNode next;


	public BuyerNode(int id, int value, ItemsList ilist) {
		this.id = id;
		this.value = value;
		this.itemsList = ilist;
	}	// end of constructor
}

class BuyersList {
	public int opt;
	private int nbNodes;
	private BuyerNode first;
	private BuyerNode last;

	public BuyersList() {
		this.first = null;
		this.last = null;
		this.nbNodes = 0;
	}	// end of constructor

	public int readFile(String filename) {

		int m = 0;
		java.io.BufferedReader br = null;

		try {
			br = new java.io.BufferedReader(new java.io.FileReader(filename));

			// Read dimensions
			String line = br.readLine();
			String[] data = line.split(" ");
			// Number of items
			m = Integer.parseInt(data[0]);
			// Number of buyers
			int n = Integer.parseInt(data[1]);
			// Optimum revenue
			this.opt = Integer.parseInt(data[2]);

			// Read Buyers Information
			int id = 0;
			while((line = br.readLine()) != null) {
				data = line.split(" ");
				// Read value
				int value = Integer.parseInt(data[0]);
				// Read Item List
				ItemsList itemsList = new ItemsList();
				for(int i = 1; i < data.length; i++)
					itemsList.append(Integer.parseInt(data[i]));
				// Insert new buyer
				this.append(id++, value, itemsList);
			}
		} catch(java.io.IOException e) {
			e.printStackTrace();
		} finally {
			try { if (br != null) br.close(); }
			catch (java.io.IOException ex) { ex.printStackTrace(); }
		}
		return m;
	}

	public boolean empty() {
		return first == null;
	}

	public int size() {
		return nbNodes;
	}

	public int totalValue() {
		BuyerNode tmp = this.first;
		int totalvalue = 0;

		// Loop this.list
		while(tmp != null){
			// Add current value to total
			totalvalue = totalvalue + tmp.value;
			tmp = tmp.next;
		}
		return totalvalue;
	}

	public int append(int id, int value, ItemsList ilist) {
		BuyerNode newNode = new BuyerNode(id, value, ilist);

		// If this.list = empty
		if(this.empty()){
			// First is the new node
			first = newNode;
		// Else
		}else{
			// Start last from first
			last = first;
			// And loop list until last node
			while ( last.next != null ){
				last = last.next;
			}
			// NewNode is the last node
			last.next = newNode;
		}
		nbNodes++;
		return 0;
	}

	private int appendNode(BuyerNode newNode) {

		// Append the node given to list
		newNode.next = this.first;
		this.first = newNode;
		nbNodes++;
		return 0;
	}

	private BuyerNode deleteNode(BuyerNode delety){
		if(this.empty()){return null;}
		BuyerNode cur = this.first;
		BuyerNode prev = this.first;

		// Loop list until delety
		while(cur != delety){
			// If cur reaches end of list, exit
			if(cur.next == null){
				return null;
			}else{
				prev = cur;
				cur = cur.next;
			}
		}
		// If cur is first
		if(cur == this.first){
			// Delete it
			this.first = this.first.next;
		// If cur is after some node
		}else{
			// Delete it
			prev.next = cur.next;
		}
		// Decrease nodes
		nbNodes--;
		return delety;
	}

	private BuyerNode selectBidder(BuyersList buy){
		BuyerNode current = buy.first;
		BuyerNode bidder = buy.first;
		float c = 0;
		float b = 0;

		// Loop list
		while(current != null){
			// value over items for current
			c = (current.value / current.itemsList.size());
			// max value over items
			b = (bidder.value / bidder.itemsList.size());
			// If there is a higher bidder
			if( c > b ){
				// He is now the bidder
				bidder = current;
			}
			current = current.next;
		}
		return bidder;
	}


	public BuyersList greedy(int m) {
		BuyerNode bidder = this.first;
		ItemsList list = new ItemsList();
		BuyersList buyers = new BuyersList();

		// Make the items list
		for(int i=1; i<=m; i++){
			list.append(i);
		}

		// While this.list of buyers is not empty
		while(!this.empty()){
			// Call select Bidder method
			bidder = this.selectBidder(this);
			// If the buyers.( items list ) is contained in list
			if(list.contains(bidder.itemsList)){
				// Remove it
				list.remove(bidder.itemsList);
				// And append the deleted node to the final list of buyers
				buyers.appendNode(this.deleteNode(bidder));
			// Else
			}else{
				// Just remove it
				this.deleteNode(bidder);
			}
		}
		return buyers;
	}

	private double getGreedyTime(String file){
		long totalTime = 0;
		// Run greedy 10 times
		for(int i=1; i<=10; i++){
			// clear list
			this.first = null;
			this.last = null;
			this.nbNodes = 0;
			// Read file
			int m = this.readFile(file);
			// Get time
			long startTime = System.nanoTime();
			this.greedy(m);
			// Get new time
			long endTime = System.nanoTime();
			// Get difference
			long timeElapsed = endTime - startTime;
			// Total time elapsed
			totalTime = totalTime + timeElapsed;
		}
		// Time in milliseconds
		return (totalTime / 1e6);
	}


	public static void main(String[] args) {

		// FILE N
		String nfiles[] = {"p200x2000.txt","p400x2000.txt","p600x2000.txt","p800x2000.txt","p1000x2000.txt"};
		System.out.println("* n = 2000 :");

		for (String file : nfiles){
			// New list
			BuyersList buy = new BuyersList();
			// Read file
			int m = buy.readFile(file);
			// Run greedy once to get resulting list
			BuyersList rez = buy.greedy(m);
			// Call getGreedyTime to get avg time
			double time = buy.getGreedyTime(file);

			// %Difference between opt and greedy
			float diff = ( (buy.opt - rez.totalValue()) / (float) buy.opt * 100 );

			// Print
			System.out.printf("- m = %4d  " , m);
			System.out.printf("avgTime : %4.2f  greedy_value : %5d  opt_value : %5d  diff : %.2f%%\n",
					time , rez.totalValue(), buy.opt, diff);
		}

		// FILE M
		String mfiles[] = {"p500x1000.txt","p500x3000.txt","p500x5000.txt","p500x7000.txt","p500x9000.txt"};
		System.out.println("\n* m = 500 :");

		for (String file : mfiles){
			// New list
			BuyersList buy = new BuyersList();
			// Read file
			int m = buy.readFile(file);
			// Run greedy once to get resulting list
			BuyersList rez = buy.greedy(m);
			// Call getGreedyTime to get avg time
			double time = buy.getGreedyTime(file);

			// %Difference between opt and greedy
			float diff = ( (buy.opt - rez.totalValue()) / (float) buy.opt * 100 );

			// Print
			String n = file.substring(5,9);
			System.out.printf("- n = %4s  " , n);
			System.out.printf("avgTime : %8.2f  greedy_value : %5d  opt_value : %5d  diff : %.2f%%\n",
					time , rez.totalValue(), buy.opt, diff);
		}
	}
}
